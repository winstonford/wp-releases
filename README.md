# wp-releases



~/git/termux-scripts (main) » ./push-wp-release.sh                                                   
🏗️  Preparing wordpress release of winstonford_com...


📂 Copying files...
✔️  Files copied.


🚛 Dumping database...
✔️  Database dumped.


📦 Tar gzipping release...
✔️  Release tgz'd.


🧹 Cleaning up...
✔️  Clean.


⚙️  Updating local git repo...
Already up to date.
✔️


⚙️  Adding release to repo and committing...
[main 205b440] release
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 winstonford_com-wordpress-2022.02.20-00.27.47.tgz
✔️  Release added and committed.


📡 Pushing release...
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 120.26 MiB | 1.32 MiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
To gitlab.com:winstonford/wp-releases.git
   6c5820f..205b440  main -> main
✔️


👽 Release complete.
Hold down the command or control key and click link below to confirm release.
https://gitlab.com/winstonford/wp-releases
---------------------------------------------------------------
